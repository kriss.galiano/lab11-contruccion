package com.tecsup.petclinic.exception;

public class VeterinarioNotFoundException extends Exception{

	private static final long serialVersionUID = 1L;

	public VeterinarioNotFoundException(String mensaje) {
		super(mensaje);
	}

}
