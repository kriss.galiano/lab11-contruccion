package com.tecsup.petclinic.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tecsup.petclinic.domain.Veterinario;
import com.tecsup.petclinic.domain.VeterinarioRepository;
import com.tecsup.petclinic.exception.VeterinarioNotFoundException;

@Service
public class VeterinarioServiceImpl implements VeterinarioService{

private static final Logger logger = LoggerFactory.getLogger(VeterinarioServiceImpl.class);
	
	@Autowired
	VeterinarioRepository repo;
	
	//Jamutaq Ortega
	@Override
	public Veterinario update(Veterinario veterinario) {
		return repo.save(veterinario);
	}
	@Override
	public Veterinario findById(long id) throws VeterinarioNotFoundException {
		Optional<Veterinario> veterinario = repo.findById(id);

		if (!veterinario.isPresent()) //si no se encuentra
			throw new VeterinarioNotFoundException("ERROR: NO SE ENCONTRO EL VETERINARIO!");
			
		return veterinario.get(); //si se encontro, que devuelva el veterinario
	}
	@Override
	public List<Veterinario> findByNombre(String nombre) {
		List<Veterinario> veterinarios = repo.findByNombre(nombre);
		veterinarios.stream().forEach(veterinario -> logger.info("" + veterinario));

		return veterinarios;
	}
	@Override
	public Iterable<Veterinario> findAll() {
		return repo.findAll(); 
	}

	
}
