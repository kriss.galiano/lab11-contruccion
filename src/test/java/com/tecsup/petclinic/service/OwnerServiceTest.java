package com.tecsup.petclinic.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tecsup.petclinic.domain.Owner;
import com.tecsup.petclinic.exception.OwnerNotFoundException;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class OwnerServiceTest {

	private static final Logger logger = LoggerFactory.getLogger(OwnerServiceTest.class);

	@Autowired
   	private OwnerService ownerService;

	//TESTS DE BUSQUEDA AUTOR: JAMUTAQ ORTEGA
	@Test
	public void testFindOwnerById() {
		long ID = 1;
		String NOMBRE = "George";
		Owner owner = null;
		
		try {
			owner = ownerService.findById(ID); //hace la busqueda
			
		} catch (OwnerNotFoundException e) {
			fail(e.getMessage());
		}
		
		logger.info(">_RESULTADO (ID) -> " + owner.getFirstName()); //muestra los resultados
		assertEquals(NOMBRE, owner.getFirstName()); //compara lo esperado con el resultado
	}
	
	@Test
	public void testFindOwnerByLastName() {
		String APELLIDO = "Davis";
		int SIZE_EXPECTED = 2;

		List<Owner> owners = ownerService.findByLastName(APELLIDO);

		logger.info(">_RESULTADOS (APE) -> " + owners.get(0).getFirstName()); //muestra los resultados
		assertEquals(SIZE_EXPECTED, owners.size());
	}
	
	@Test
	public void testFindOwnerByFirstName() {
		String NOMBRE = "Betty";
		int SIZE_EXPECTED = 1;

		List<Owner> owners = ownerService.findByFirstName(NOMBRE);

		logger.info(">_RESULTADOS (NOM) -> " + owners.get(0).getFirstName()); 
		assertEquals(SIZE_EXPECTED, owners.size());
	}
	
	//TESTS DE CRUD
	@Test
	public void testCreateOwner() { //AUTOR: KRISS GALIANO
		String NOMBRE = "Kriss";
		String APELLIDO = "Galiano";
		String CIUDAD = "Huanuco";

		Owner owner = new Owner(NOMBRE, APELLIDO, CIUDAD); //crea un objeto
		owner = ownerService.create(owner); //crea el owner con los datos pasados
		logger.info(">_OWNER INSERTADO -> " + owner.getFirstName());

		//hace las compaaciones
		assertThat(owner.getId()).isNotNull();
		assertEquals(NOMBRE, owner.getFirstName());
		assertEquals(APELLIDO, owner.getLastName());
		assertEquals(CIUDAD, owner.getCity());
	}
	
	@Test
	public void testUpdateOwner() { //AUTOR: PERCY HERRERA
		String NOMBRE = "Percy";
		String APELLIDO = "Herrera";
		String CIUDAD = "Moquegua";
		long create_id = -1;

		//nuevos datos
		String NUEVO_NOMBRE = "Percy_nuevo";
		String NUEVO_APELLIDO = "Herrera_nuevo";
		String NUEVA_CIUDAD = "Moquegua_nuevo";

		//crea un objeto owner con los datos
		Owner owner = new Owner(NOMBRE, APELLIDO, CIUDAD); 
		logger.info(">_OBJETO OWNER CREADO -> " + owner.getFirstName());
		
		//inserta en la bd el owner y retorna el objeto a ownerBD
		Owner ownerBD = ownerService.create(owner); 
		logger.info(">_OBJETO OWNER INSERTADO -> " + ownerBD.getFirstName());

		create_id = ownerBD.getId(); //obtiene el ID del owner que se inserto en la BD

		//se asignan los nuevos datos al objeto ownerBD
		ownerBD.setLastName(NUEVO_APELLIDO);
		ownerBD.setFirstName(NUEVO_NOMBRE);
		ownerBD.setCity(NUEVA_CIUDAD);

		//ejecuta el update, con los nuevos datos asignados
		Owner upgradeOwner = ownerService.update(ownerBD);
		logger.info(">_OBJETO OWNER ACTUALIZADO -> " + upgradeOwner.getFirstName());

		//compara los resultados que estan en la BD con lo que se asignó
		assertThat(create_id).isNotNull();
		assertEquals(create_id, upgradeOwner.getId());
		assertEquals(NUEVO_APELLIDO, upgradeOwner.getLastName());
		assertEquals(NUEVO_NOMBRE, upgradeOwner.getFirstName());
		assertEquals(NUEVA_CIUDAD, upgradeOwner.getCity());
	}
	
	@Test
	public void testDeleteOwner() { //AUTOR: BYAN SEGURA
		String NOMBRE = "Bryan";
		String APELLIDO = "Segura";
		String CIUDAD = "Lima";

		//crea un nuevo objeto con los datos
		Owner owner = new Owner(NOMBRE, APELLIDO, CIUDAD);
		owner = ownerService.create(owner); //inserta en la BD
		logger.info(">_OWNER INSERTADO (D) -> " + owner.getFirstName());

		try { //elimina el objeto insertado por su id
			ownerService.delete(owner.getId());
			logger.info(">_OWNER ELIMINADO -> " + owner.getFirstName());
			
		} catch (OwnerNotFoundException e) {
			fail(e.getMessage());
		}
			
		try { //busca en la bd el objeto eliminado
			ownerService.findById(owner.getId());
			assertTrue(false); //no debe de devolver ningun dato
			
		} catch (OwnerNotFoundException e) {
			assertTrue(true);
		} 				

	}

}
