package com.tecsup.petclinic.domain;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VeterinarioRepository  extends CrudRepository<Veterinario, Long> {
	List<Veterinario> findByNombre(String nombre); //busca veterinarios por nombre
}
