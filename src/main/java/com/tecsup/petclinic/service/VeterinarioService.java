package com.tecsup.petclinic.service;

import java.util.List;

import com.tecsup.petclinic.domain.Veterinario;
import com.tecsup.petclinic.exception.VeterinarioNotFoundException;

public interface VeterinarioService {

	
	//Jamutaq Ortega
	Veterinario update(Veterinario veterinario);
	Veterinario findById(long id) throws VeterinarioNotFoundException;
	List<Veterinario> findByNombre(String firstName);
	Iterable<Veterinario> findAll();
		
}
